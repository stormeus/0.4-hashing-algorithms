//
// SQMain: This is where the module is loaded/unloaded and initialised.
//
//	Written for Liberty Unleashed by the Liberty Unleashed Team.
//

#pragma once
#include <string.h>
#include "SQModule.h"
#include "plugin.h"

#ifdef WIN32
	#define EXPORT __declspec(dllexport)
#else
	#define EXPORT
#endif

// Squirrel
extern "C"
{
	#include "squirrel.h"
}